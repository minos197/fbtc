# README #

Windows pre-build executable is included in download section.

### What is this repository for? ###

* Facebook Terminal Chat is small python [SleekXMPP](https://github.com/fritzy/SleekXMPP) implementation of a facebook chat client.
* It was intentionally made to extract basic information from jabber stanzas and handle the information internally, to make it easier to upgrade when Facebook removes XMPP support (April 2015)
* It implements custom terminal input handlers to overcome Unicode issues with Windows machines and python 2.7.X, and allow injecting text without affecting user's typing.When event driven text is pushed in the file descriptor, user text is cleared,incoming text is printed and user text is appended in a new line.
* Version 0.1 ( Very Aplha )


### How do I get set up? ###


* [Executable requires Microsoft Visual C++ 2008 SP1 Redistributable Package](http://www.microsoft.com/en-us/download/details.aspx?id=5582)
* [Requires GTK3 libs](http://sourceforge.net/projects/pygobjectwin32/files/o)
* Build for python 2.7.xx
* Comes with instructions


### Who do I talk to? ###

* Minos197
* minos197@gmail.com