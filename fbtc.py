#!/usr/bin/env python

from __future__ import print_function
import signal
import sys
import os
import time
import platform

from modules.facebot  import FaceBot
from modules.config   import ConfigManager
from modules.command  import CommandManager

if platform.system() == 'Windows':
  host_platform = 'Win'
elif platform.system() == 'Linux':
  host_platform = 'Lin'
else:
  print ("Not a supported operating system,exiting")
  sys.exit(0)

def first_split(input,token):
   '''Split that is immune to infinite repetition of first token'''
   a = input[:input.find(token)+(len(token))]
   b = input[input.find(token)+(len(token)):]
   a = a.replace(token,'')
   if a:return[a,b]
   else:return[b]

def signal_handler(signal, frame):
  print ("Detected CTRL+C")

  #fb.disconnect(wait=True)
  fb.quit()
  print ("Disconnected.Press any key to exit")
  sys.exit(0)

if __name__ == '__main__':

  #Initialize modules
  signal.signal(signal.SIGINT, signal_handler)
  
  #Set current path to where the file is located
  os.chdir(os.path.dirname(os.path.realpath(__file__)))
    
  #Read Configuration
  config    = ConfigManager()
  u,p = config.get_config()
  lang = ("Greek","iso-8859-7")#Todo make the language be configured through the config manager

  fb = FaceBot(u,p,lang)
  fb.begin()
  prompt = ''

  #####################
  # Helper Functions  #
  #####################
  def test():
    pass

  def cls():
    if host_platform == 'Win': os.system('cls')
    else : os.system('clear')
    fb.fprint('',False,False)

  def quit():
    fb.quit()
    sys.exit()

  def cbot(var):
    text = first_split(var,'notify ')[-1]
    bot ={'enable':True,'*':{'*':text}}
    fb.attach_chatbot(bot)
    fb.fprint("",False,False)

  def alive():
    text = "Last event was %.3f seconds ago."%fb.get_alive()
    fb.fprint(text,False,True)

  def logout():
    fb.quit()
    try:
      os.remove("pwin.dll")
    except:
      print ("No log-in information stored in system")
    sys.exit()

  def help():
    """Printout a command list for user """
    commands = [ ("cls"      ,                            "Clear Screen"),\
                 ("(quit/q)" ,                            "Quit the chatbot"),\
                 ("(find/f)",                             "Find roster names mathing input"),\
                 ("(online/o)",                           "Show on-line user list"),\
                 ("(send/s) (name /alias / id) message",  "Send a chat message to user"),\
                 ("(alias/a) name chosenalias",           "Set an alias for faster name selection"),\
                 ("(reply/r) message",                    "reply to the sender of the last message"),\
                 ("(history/h) name",                     "Show conversation with user"),\
                 ("(notify/n)  name",                     "Add/remove notification message on user join"),\
                 ("(reply/r) message",                    "reply to the sender of the last message"),\
                 ("(status/st)",                          "Show application status"),\
                 ("(lstevent/l)",                         "Show time since last even from fb server"),\
                 ("(noise/n) noisefile",                  "Add a noise file to hide the chat in random text"),\
                 ("autoreply botconfig (not implemented)","Set an autoreply bot based on configuration file"),\
                 ("about",                                "Show basic credits"),\
                 ("roster",                               "Show full friend list roster."),\
                 ("logout",                               "Remove all save information and exit")
    ]
    
    #Only add codepage related routines in windows host
    if host_platform == 'Win':
      commands.append( ("langls",                               "List all supported languages") )
      commands.append( ("langdir language",                     "List all supported codepages for given language") )
      commands.append( ("langset codepage",                     "Set the console codepage to codepage") )
      
    help_txt =( "/-%s%s%s%s\\\n|%s|"%( "[ Command ]", '-'*28, "[ Description ]", '-'*36, " "*92 ) )
    for entry in commands:
      l = 40-len(entry[0])
      k = 91 -l -len(entry[0]) -len(entry[1])
      help_txt +=( "\n| %s%s%s%s|" %( entry[0], " "*l, entry[1], " "*k ) )
    help_txt += ( "\n\\-%s/"%('-'*91) )
    fb.fprint(help_txt)

  #Function Table, aligning user commands to system functions
  cmd = { "cls"       :cls,\
          "quit"      :quit,\
          "q"         :quit,\
          "send"      :fb.msg_push,\
          "s"         :fb.msg_push,\
          "reply"     :fb.reply,\
          "r"         :fb.reply,\
          "alias"     :fb.setalias,\
          "a"         :fb.setalias,\
          "history"   :fb.get_history,\
          "h"         :fb.get_history,\
          "online"    :fb.show_online,\
          "o"         :fb.show_online,\
          "find"      :fb.search_roster,\
          "f"         :fb.search_roster,\
          "notify"    :fb.attach_notify,\
          "n"         :fb.attach_notify,\
          "about"     :fb.get_credits,\
          "autoreply" :cbot,\
          "noise"     :fb.add_noise,\
          "status"    :fb.get_status,\
          "st"        :fb.get_status,\
          "lstevent"  :alive,\
          "l"         :alive,\
          "help"      :help,\
          "logout"    :logout,\
          "roster"    :fb.show_roster,\
          "test"      :test
  }
  
  #Only add codepage related routines in windows host
  if host_platform == 'Win':
    cmd["langset"]  = fb.set_kb_language
    cmd["langdir"]  = fb.cpmanager.dir_lang
    cmd["langls"]   = fb.cpmanager.list_lang


  #initialize the command manager using the previous table
  cmdm = CommandManager(cmd)
  start_time = time.time()

  ######################
  #  Main Program Loop #
  ######################
  while(1):
    #wait for the client to indicate successful login
    if not fb.ready:
      time.sleep(1)
      #if it takes a long time kill the program
      if time.time() - start_time > 20.0:
        print ("Login Time-out.Ensure you are using your FaceBook user-name and the correct password")
        uinput = ''
        while uinput not in ['e','r']:
          uinput = raw_input("Clean logging and exit ( e ) /Retry? ( r )")
        #if retry restart timer
        if uinput == 'r':
          start_time = time.time()
          continue
        os.remove('pwin.dll')
        fb.quit()
        sys.exit()
      continue
    try:
      #because of windows broken way of handling unicode it is better to use one routine to print data to stdout
      if host_platform == 'Win':uinput = fb.kbm.premptive_input( "?", fb.fprint )
      else:uinput = fb.kbm.premptive_input( "?" )
      if not uinput:continue

    #Capture user press of CTRL+C
    except KeyboardInterrupt :
      fb.quit()
      sys.exit()

    if host_platform == 'Win':
      #decode the input form set language to bytes
      uinput= fb.cpmanager.send_string_reencode(uinput)

    #parse the string and execute the corresponding function from the defined table
    cmdm.parse(uinput)
