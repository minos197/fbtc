#!/usr/bin/env python

import threading
import time

class HeartBeatManager( threading.Thread ):
    '''Custom timer that support thread exit when stopped'''

    def __init__ (self, period, callable, *args, **kwargs):
        super(HeartBeatManager, self).__init__()
        self._stop    = threading.Event()
        self.period   = period
        self.args     = args
        self.callable = callable
        self.kwargs   = kwargs
        self.daemon   = True

    def stop(self):
      '''Stop the thread'''

      self._stop.set()

    def stopped(self):
      '''return the thread status'''

      return self._stop.isSet()

    def run(self):
        while not self.stopped():
            self.callable(*self.args, **self.kwargs)
            time.sleep(self.period)