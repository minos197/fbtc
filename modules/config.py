#!/usr/bin/env python

import os

class ConfigManager():
  """ Module that controls the program configuration and file I/O"""

  def __init__(self, asciif = 'credentials.txt', binf = 'pwin.dll'):
    self.ascii_file = asciif
    self.bin_file   = binf
    self.username, self.password = self.read_creds()

  def get_config( self ):
    ''' Return username and password to caller '''

    return (self.username, self.password)

  def cleanup ( self ):
    '''Delete the files used in the config manager'''

    if os.path.isfile(self.ascii_file):
      os.remove(self.ascii_file)
    if os.path.isfile(self.bin_file):
      os.remove(self.bin_file)

  def save_creds(self,username,password):
    '''Save data in a binary slightly obfuscated format'''

    def shift(input):
      out = ''
      for i in input:
        newval = ord(i) + 128
        out += chr(newval)
      return out

    user  = shift(username)
    passw = shift(password)

    with open(self.bin_file, 'wb') as f:
      entries = f.write(chr(7)+user+chr(10)+passw)
    f.close()
    return

  def read_creds( self ):
    '''Check for configuration existence and return user password if there is a config file'''

    #set the default name for binary  file
    binf = 'pwin.dll'

    #if the ascii file exists
    if os.path.isfile(self.ascii_file):
      with open(self.ascii_file, 'r') as f:     
        entries = f.readlines()
      f.close()
      for e in entries:
        name,value = e.split(":")
        if name == "username": user= value.replace('\n','')
        elif name == "password": passw = value.replace('\n','')
        #if the user did not input the proper format attempt to correct it
        if "@chat.facebook.com" not in user: user += "@chat.facebook.com"

      #delete the file after it has been used
      os.remove(self.ascii_file)

      #save it as a binary file
      self.save_creds(user,passw)
      return (user,passw)

    #If binary file exists
    elif os.path.isfile(self.bin_file):
      def shift(input):
        out = ''
        for i in input:
          newval = ord(i) - 128
          out += chr(newval)
        return out

      with open(self.bin_file, 'rb') as f:
        data = f.read()
      f.close()

      #extract the binary data
      indx = data.find('\n')

      #remove bell
      username = data[1:indx]
      #remove newline
      password = data[indx+1:]

      user  = shift(username)
      passw = shift(password)
      return (user,passw)
    #Get them from the user
    else:
      from getpass import getpass
      print("No credentials found")
      while True:
        user = raw_input("Username?")
        passw = getpass()
        #Attempt to correct wrongly formatted user name
        if "@chat.facebook.com" not in user and "@" not in user: user += "@chat.facebook.com"
        if "@chat.facebook.com" not in user and "@" in user:
          print "Please use the facebook username instead of your email"
          continue
        self.save_creds(user,passw)
        os.system('cls')
        break
    return (user,passw)


