#!/usr/bin/env python

from __future__ import print_function
import threading

class TerminalBell(threading.Thread):
  '''Terminal Audio notification module that runs on a separate thread'''

  def __init__(self):
    super(TerminalBell, self).__init__()

  def ring(self):
    '''Print bell character creating a simple tune'''

    print('\x07\x07\x07',end ="")
    return