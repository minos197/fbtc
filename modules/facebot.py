#!/usr/bin/env python

"""Facebot.py: An XMPP client for facebook chat."""

from __future__ import print_function

__author__  = "Minos Galanakis"
__license__ = "LGPL"
__version__ = "2.1"
__email__   = "minos197@gmail.com"

#import standard libs

from random import randint
import sleekxmpp
import time
import platform
import sys

#Import modules
from rosterparser import RosterParser
from heartbeat import HeartBeatManager
from notifications import TerminalBell
from codepage import CodePageManager

if platform.system() == 'Windows':
  from modules.winkeyboard import KeyboardHandler
  host_platform = 'Win'
elif platform.system() == 'Linux':
  from modules.linkeyboard import KeyboardHandler
  host_platform = 'Lin'
else:
  print ("Not a supported operating system,exiting")
  sys.exit(0)

class FaceBot(sleekxmpp.ClientXMPP):
  ''' Facebook chat client class extending sleekxxmp client'''

  def __init__( self, username, password, keybrdlang ):

    #Add the handlers
    self.use_ssl = False
    sleekxmpp.ClientXMPP.__init__(self, username,password)
    self.add_event_handler('message', self.srv_event_message)
    self.add_event_handler('got_online', self.srv_event_got_online)
    self.add_event_handler('changed_status', self.srv_event_changed_status)
    self.add_event_handler("session_start", self.srv_event_session_start, threaded=True)

    if host_platform == 'Win':
      self.cpmanager  = CodePageManager( "codepages.ini" , keybrdlang)
      self.kbm = KeyboardHandler( self.cpmanager.get_codepage(), self.pop_msg_from_queue )
    else:
      self.kbm = KeyboardHandler( self.pop_msg_from_queue )
    self.heartbeat    = HeartBeatManager(30.0,self.heartbeat_callback)

    #Add variables
    self.chatbot          = {'enable':False}
    self.noise            = []
    self.present          = []
    self.msg_queue        = []
    self.last_msg_sender  = ''
    self.auto_reconnect   = True
    self.rstr             = None
    self.ready            = False
    self.last_event_stmp  = 0.0
    self.rster            = None
    self.rstergen         = None
    self.hostos           = platform.system()[:3]


  def begin(self):
    '''Initialize the facebok client'''

    #Set the server and connect
    server = ('chat.facebook.com', 5222)
    self.connect(server)

    #Set it to non blocking mode
    self.process(block=False)

    #Notify the server of your presence
    self.send_presence()

    #Start the heartbeat daemon
    if not self.heartbeat.isAlive():
      self.heartbeat.start()


  def restart(self):
    '''Attempt to reconnect to server without re-instantiating it'''

    self.last_event_stmp = 0.0
    self.ready = False
    self.rster['online'] = {}
    self.reconnect()
    self.fprint("Restarted")

  def quit(self):
    ''' Clean before quitting the client'''

    self.kbm.quit()
    if host_platform == 'Win' : self.cpmanager.quit()
    self.disconnect(wait=True)
    self.heartbeat.stop()


  ###############
  #  Utilities  #
  ###############

  def fprint(self,*args ):
    '''Function that uses system set os to print to console'''

    if host_platform == 'Win':
      ( lambda: self.cpmanager.uni_print( *args ) )()
    else:
      lnend = '\n'
      if len(args) > 1:
        lnend = "%s"%(int(args[1])*'\n')
      print(args[0],end=lnend)

  def get_name( self,word ):
    '''Attempt to retrieve name and JID from an uknown type of input'''

    #if the user is trying to sned a  message by id
    if word.isdigit():
      for n in self.rster['id'].keys():
        if self.rster['id'][n] == int(word):
          jid = self.rster['jid'][n]
          name = n
          break
    #if the destination is an alias
    elif word in self.rster['alias'].keys():
      jid = self.rster['jid'][ self.rster['alias'][word] ]

      #change to into the non aliased jid
      name = self.rster['alias'][word]
    else:
      try:
        jid = self.rster['jid'][word]
        name = word
      except KeyError:
        self.fprint("Name %s not found in roster"%repr(word))
        return ('','')
    return(name,jid)


  def search_roster(self,partial_name):
    '''Find names that contain the partial input text'''

    hits = []
    self.fprint("Search "+partial_name)
    if self.rster:
      for n in self.rster['jid'].keys():
        if partial_name in n:
          self.fprint("%s: %s with ID:%s"%("Found Match",n,self.rster['id'][n] ))
          hits.append((self.rster['id'][n],n))
      if not  hits: self.fprint( " Did not find anything that matches \"%s\" query."% partial_name )
      return hits


  def add_noise(self,file):
    '''open a file and  add text between chat messages'''

    #if the list already exists reset it
    if len(self.noise):
      self.noise = []
      return

    #if not then addd the noise
    with open(file, 'r') as f:
      data = f.readlines()
    f.close()
    for line in data:
      self.noise.append(line[:-1])


  def fakesend_msg(self,to,msg):
    '''Debug routine to echo send messages back to the receive queue'''

    import time
    fmsg = {'type':'chat','body':msg,'from':to}
    time.sleep(2)
    self.srv_event_message(fmsg)


  def set_kb_language( self, lang = '', codepage = '' ):
    '''intermediate function that changes language while notifying the keyboard manager for the event'''
    
    self.cpmanager.set_codepage( lang,codepage )
    #update the keyboard parser
    if self.hostos == 'Win':
      self.kbm.set_charmap( self.cpmanager.get_codepage() )


  def read_chatbot(self,file):
    '''Read a file for automated reply control bot'''

    with open(file, 'rb') as f:
      data = f.readlines()
    f.close()
    output = {}
    for line in data:
     userd = {}
     user,text = first_split(var,':')
     incoming,reply = first_split(text,':')
     userd[incoming] = reply
     output[user] = userd
    return output


  ############################
  # Server triggered methods #
  ############################

  def srv_event_session_start(self,event):
    '''Called when the client confirms handshake with server'''

    self.rstr = self.get_roster()
    self.fprint ("Client Online")
    #only run roster generator once
    if not self.rstergen:
      self.rstergen = RosterParser(self.rstr,self.parsing_callback)
      self.rstergen.build()
    self.ready = True


  def srv_event_got_online(self,event):
    '''Sever event triggered when a user gets online'''
    pass


  def srv_event_changed_status(self,event):
    '''Server event triggered when a user status changes'''

    if not self.rster:
      self.present.append(str(event['from']))
      self.rster = self.rstergen.get()
      if not self.rster:  return #If roster is being processed do nothing

    self.last_event_stmp = time.time()
    #print ("Changed Status")
    jid =  event['from']
    status = event['type']
    name = self.rster['name'][jid]

    if status == 'available':
      self.rster['online'][name] =  time.time()
      if name in self.rster['notify']:
        self.fprint(name + ' is online')
    else:
      if name in self.rster['notify']:
        self.fprint(name + ' went offline')
      self.rster['online'].pop(name)

  def srv_event_message(self,msg):
    ''' Server event handling incoming messages'''

    #Check if there is a noise data stream
    ln = len(self.noise)
    if ln:
      lind = randint(0,ln)
      noise = self.noise[lind]
    else:
      noise = ''

    if msg['type'] in ['chat','normal']:

      msg_sender = msg['from']

      #Attempt to re-encode the name text if they are Unicode
      msg_sender_name =  self.rster['name'][msg_sender]

      #print('msg received2')
      if self.hostos == 'Win': msg_text = self.cpmanager.recv_string_reencode( msg['body'] )
      else: msg_text = msg['body']
      #store the message
      self.rster['msgs'][msg_sender_name].append((msg_text,time.time(),0))

      #print('msg received3')
      #store the name to allow replies
      self.last_msg_sender = msg_sender_name

      #Change the name with the alias if set
      if msg_sender_name in self.rster['invalias'].keys():
        msg_sender_name =  self.rster['invalias'][msg_sender_name]

      #print it on the screen
      prefix = '%s  =>'%str(msg_sender_name)
      self.msg_queue.append(str("%s> %s %s"%(prefix,msg_text,noise)))

    else:
      self.msg_queue.append("MSG Type: " +str(msg['type']))
      #botprint("MSG Type: " +str(msg['type']))
    TerminalBell().ring()


  ############################
  #  User triggered methods  #
  ############################

  def msg_push( self,to,msg ):
    '''Send a message through the facebook bot'''

    name,jid = self.get_name(to)

    if name:
      if name in self.rster['online'].keys():
        c_prefix = " Sending to on-line contact -> "
      else:
        c_prefix = " Sending to off-line contact -> "

      #send the message
      self.send_message(mto=jid,mbody=msg,mtype='chat')
      #self.fakesend_msg(jid,msg)
      #notify the user of success
      self.fprint("\n"+c_prefix+"Sent\r",False,False)
      time.sleep(1)
      #clear the notification
      self.fprint("%s\r"%(' '*40),False,False)
      self.rster['msgs'][name].append( (msg, time.time(), 1) )
      return True
    return False


  def reply(self,msg):
    '''Reply to sender of last incoming message'''

    if not self.last_msg_sender:self.fprint("No-one to reply to")
    else:self.msg_push(self.last_msg_sender,msg)


  def get_status(self):
    '''Print some basic information on screen'''
    
    #Create the message in a string variable
    status = "[ Notify List ]"
    for n in self.rster['notify']:
      status += ("\n%s"%n)
    status += ("\n[ Aliases ]")
    for key in self.rster['alias'].keys():
      status += ( "\n%s as: %s"%( self.rster['alias'][key],key ))
    status += ("\nChatbot is %s"%['disabled','enabled'][int(self.chatbot['enable'])])
    status += ("\nReply Sender: %s"%self.last_msg_sender)
    status += ("\nLast event from server was %s seconds ago"%str(self.get_alive()))

    #Send it
    self.fprint(status)


  def attach_chatbot(self,rules):
    '''Enable the chatbot'''

    from copy import deepcopy
    self.chatbot = deepcopy(rules) 
    self.chatbot['enable'] = True


  def get_alive(self):
    '''Return the time since last event from server'''

    if self.last_event_stmp: return (time.time() - self.last_event_stmp )
    else: return -1


  def get_history(self,user):
    '''Get message history  for a specified user/id/alias'''
    
    #retrieve user name and jid from argument
    name,id = self.get_name(user)
    
    #Find and 
    try:
      if not name: raise KeyError
      msg_list = self.rster['msgs'][name]
      #if list is empty exit
      if not msg_list:
        print("No conversation history found")
        return
    except KeyError:
      return
    
    #format history in one big string and print it
    #get fixed variables
    l = len(name)
    prefix = ['%s  =>  '%str(name),"me %s<=  "%((l-1)*' ')]

    #add header
    history = "[%sConversations with %s %s]\n[%s]\n"%("-"*30,name,"-"*30," "*(80+l) )

    #iterate though the messages and add them to string
    for m in msg_list:
      m_rem = ''
      who = m[2]
      what = m[0]
      if len(what) > 65:
        m_rem = m[0][65:]
        what = m[0][:65]
      k = len(what)+l
      history += ("[  %s%s%s]\n"%(prefix[who],what,' '*(91-k)))

      #Implement pseudo word wrapping
      if m_rem:
        while len(m_rem)>65:
          what = m_rem[:65]
          m_rem = m_rem[65:]
          history += ("%s%s\n"%(" "*24,what))
        history += ("%s%s\n"%(" "*24,m_rem))

    #add footer
    history += ("[%s]"%("-"*(80+l)) )
    self.fprint("%s"%( history ))


  def attach_notify(self,name):
    ''' Allow verbose reporting of users '''

    if name not in self.rster['notify']:
      self.rster['notify'].append(name)
      self.fprint("Attached notifications to %s"%name)
    else:
      self.rster['notify'].remove(name)
      self.fprint("Removed notifications to %s"%name)


  def show_online(self):
    ''' Display a list of on-line users '''

    screen_text = "%s[ Showing online users ]"%( " "*22 )
    screen_text += "\n|%s%s%s%s%s%s\n%s"%(" ID",("  ") + "| username"," "*30,"|got online|"," ", "for time |","-"*69)
    if not self.rster['online']: return #Do not proccess if no changed status event has happened
    for item in self.rster['online'].keys():

      t = time.strftime("%H:%M:%S",time.gmtime(self.rster['online'][item]))

      #todo make data stored in roster to be forced Unicode
      n = unicode(item)

      l = len(n)
      try:
        id = self.rster['id'][item]
      #those try will be removed when the type is forced unicode
      except TypeError:
        id = -1
        self.fprint ("TypeError")
      except KeyError:
        self.fprint ("KeyError")
        try:
          id = self.rster['id'][str(item)]
        except KeyError:
          self.fprint ("SOMETHING IS VERY WRONG",repr(item))
      try:
        online_time = (time.time()-self.rster['online'][item])
      except KeyError:
        online_time = (time.time()-self.rster['online'][str(item)])
        self.fprint ("KeyError2")
      online_time = time.strftime("%H:%M:%S",time.gmtime(online_time))
      screen_text += ("\n| %.3d | %s %s| %s | %s |")%(id,n," "*(37-l),t,online_time)
    #print(screen_text)
    #print the fully constructed list
    self.fprint(screen_text)


  def show_roster( self ):
    ''' Print a visible list of all the users in friend list '''

    names = self.rster['id'].keys()
    names.sort()

    line_cnt    = 0
    block_cnt   = 0
    line        = ''

    #Create the Title
    screen_text = u"%s[ Showing all users ]\n%s"%( " "*45,"-"*120 )

    #got through the names in alphabetical order
    for n in names:
      id = self.rster['id'][n]
      ll = 40 - len(unicode(n))
      line +=  u"%3d %s %s"%( id, n," "*ll )
      line_cnt += 1

      #every three words make a line
      if line_cnt == 3:
        screen_text += u"\n%s"%(line)
        block_cnt += 1
        line_cnt = 0 
        line = ''

      #Print in blocks in order not to hit the memory limit for a string object
      if block_cnt == 20:
        self.fprint( screen_text.rstrip() ,False )
        screen_text = ''
        block_cnt = 0

    #if there are names left print them
    if line:
      screen_text += ("\n%s")%(line)

    #print the last remaining lines
    self.fprint( screen_text )


  def setalias(self,name,alias):
    ''' Allow re-addressing user names to aliases'''

    if name in self.rster['jid'].keys() and not alias in self.rster['jid'].keys():
      self.rster['alias'][alias] = name
      self.rster['invalias'][name] = alias
      self.fprint("Alias Set as %s->%s"%(name,alias) )


  def get_credits(self):
    '''Display basic credits'''

    credits = "Command Line Facebook chat v 0.1Apha\nCreated by:Minos Galanakis"
    self.fprint(credits)


  ###############
  #  Callbacks  #
  ###############

  def pop_msg_from_queue( self ):
    '''Get the oldest message from the rx buffer'''

    try:
      output = self.msg_queue.pop(0)
    except IndexError:
      output = ''
    return output


  def heartbeat_callback(self):
    '''Function that is called from the heartbeat manager when it expires'''

    if not self.ready: return
    if self.get_alive() > 200.0:
      self.fprint("Server Appears to be off-line,please wait for retry")
      self.restart()


  def parsing_callback(self):
    '''Callback from roster parser to update the roster when its fully processed'''

    self.fprint("Roster processing complete")

    #store the roster
    self.rster = self.rstergen.get()

    #add the users who stated presence on startup/before rostergen
    strup =  time.time()

    for i in self.present:
      usr_name = self.rster['name'][i]
      self.rster['online'][usr_name] = strup

