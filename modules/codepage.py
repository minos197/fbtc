#!/usr/bin/env python

import os
import sys

class CodePageManager():
  def __init__(self,cp_file, def_cp):
    self.set_echo_file_name()
    self.codepages = self.load_codepages(cp_file)
    lang,cp = def_cp
    self.bootup_cp = self.get_sys_codepage()
    self.set_codepage( lang , cp )

  def set_echo_file_name(self):
    ''' Helper function to select a temporary echo file for each instance of fbc'''

    self.echofname = "NULL"
    i = 0
    while os.path.exists(self.echofname):
      self.echofname = "NULL%d"%i
      i+= 1

  def get_sys_codepage( self ):
    '''Return the system codepage'''

    return os.popen('chcp').read()[18:-1]


  def uni_print( self, text , newline = True, change_cp = True):
    '''Print by setting the terminal to Unicode and ignore pipe encoding (using bytes)'''

    #set the newline
    nl = '\n'
    if not newline: nl = ''
    if change_cp:os.system("chcp %d >> %s"%(65001,self.echofname) )

    #Suppress Unicode errors and flush buffers after print
    try:
      sys.stdout.write("\r%s%s"%(text.encode('utf-8'),nl))
      sys.stdout.flush()
    except IOError:
      sys.stdout.flush()
      sys.stdin.flush()
    if change_cp: self.reset_codepage()

  def uni_print_lines(self, line_arr , newline = True):
    '''Print consecutive lines without toggling terminal code page at each iteration'''

    #set the terminal in Unicode mode
    self.set_codepage('Unicode','utf-8', True)

    #print all the lines
    for line in line_arr:
      self.uni_print( line, newline )

    #restore terminal
    self.reset_codepage()

  def set_codepage( self, lang = '', codepage = '', mute = False ):
    '''Set the windows console codepage '''

    def set_language( lang ):
      if lang in self.codepages.keys():
        self.language = lang
        return lang
      else: return ''

    #find the language
    lng = set_language(lang)
    if lng:
      #find the codepage
      cp = self.codepages[lng]
      for c in cp:
        if c['name'] == codepage:
          os.system("chcp %s >> %s"%(c['codepage'], self.echofname))
          self.current_cp = c['codepage']
          self.cur_cp_name = c['name']
          if not mute: print("\nCodepage Set to %s"%(self.cur_cp_name))
          return True
        else: continue
      return False
    else:
      return False

  def reset_codepage( self ):
    '''Restore the last non Unicode used codepage'''

    os.system( "chcp %s >> %s"%( self.current_cp, self.echofname ))


  def get_codepage( self ):
    '''Return the current codepage by name'''

    return self.cur_cp_name

  def load_codepages( self, filename ):
    ''' Read codepages from a file into a dictionary '''

    codepage = {}
    with open(filename,'r') as f:
      data = f.readlines()
    f.close()

    for line in data:
      cp,name,info,lang = line[:-1].split(' , ')
      if '-' in lang:
        langs = lang.split('-')
        for l in langs:
          try:
            codepage[l].append( {'codepage':cp,'name':name,'info':info} )

          except KeyError:
            codepage[l] = [{'codepage':cp,'name':name,'info':info}]
      else:
        try:
          codepage[lang].append({'codepage':cp,'name':name,'info':info} )
        except KeyError:
          codepage[lang] = [ {'codepage':cp,'name':name,'info':info} ]
    return codepage

  def detect_unicode(self,text):
    ''' Rudimentary method of detecting unicode chars in a string'''

    try:
      text.decode('ascii')
      return False
    except UnicodeDecodeError:
      return True

  def detect_unicode_new( self, text ):
    ''' Detect unicode in a string by calculating how many chars cannot be printed'''

    rep = repr(text)
    if len(rep) == len(text)+ 2: return False
    else: return True

  def list_lang( self ):
    ''' Print all the included languages '''

    for l in self.codepages.keys():
      print("%s"%(l))

  def dir_lang(self,language):
    ''' Print all the codepages contained in a language '''

    try:
      lang = self.codepages[language]
    except KeyError:
      print("Language Not found")
      return 

    for l in lang:
      print("%s : %s"%(l['name'],l['info']))

  def recv_string_reencode(self,raw_string):
    '''Convert an string to bytes'''

    enc_string = raw_string
    try:
      raw_string.decode('ascii')
    except UnicodeDecodeError:
      enc_string = raw_string.decode('utf-8')
    return enc_string

  def send_string_reencode( self, raw_string ):
    '''Convert an string to bytes'''

    try:
      enc_str = raw_string.decode( 'ascii' )
    except UnicodeDecodeError:
      enc_str = raw_string.decode( self.cur_cp_name )
    return enc_str

  def quit(self):
    '''Clean-up'''
    #restore original codepage of terminal
    os.system("chcp %s"% self.bootup_cp )

    #delete the intermediate file used in suppressing echo
    os.remove( self.echofname )
