#!/usr/bin/env python

"""keyboard.py: A keyboard handler for python."""

from __future__ import print_function

__author__  = "Minos Galanakis"
__license__ = "LGPL"
__version__ = "2.1"
__email__   = "minos197@gmail.com"

#import standard libs
import os
import sys
import msvcrt
import time
import threading
from collections import deque as cbuffer

class KeyboardHandler( ):

  def __init__( self , cm ,cb ):
    self.line      = ''
    self.cb        = cb
    self.newline   = False
    self.stop      = False
    self.set_charmap( cm )
    self.command_buffer = cbuffer( maxlen= 50 )

  def set_charmap( self, map ):
    '''Update the current character map variable'''

    self.charmap = map

  def quit( self ):
    '''Clean up routines'''

    #signal the thread to stop
    self.stop = True

    #unblock the getch function
    msvcrt.ungetch(' ')

  def clear_chars( self, number ):
    ''' Clear previously typed chars in terminal'''

    whitespace = " "* (number+1)
    sys.stdout.write( '\r'+ whitespace +'\r' )

  def premptive_input( self , message , print_cb ):
    ''' Asks for user input but allows main program to inject text in it'''

    #print the prompt without adding a newline
    sys.stdout.write( message )

    #start the keyboard query manager as a thread
    self.newline   = False
    thread = threading.Thread(target=self.get_keystroke,args = message)
    thread.start() 

    #loop until user presses enter
    while True:
      #self.get_keystroke().next()
      inc_msg = self.cb()
      if inc_msg:

        self.clear_chars( len(self.line) )
        sys.stdout.flush()

        #Ensure the message contains a byte decoded version of the string
        message = "%s\n%s%s"%(inc_msg,message,self.line.decode(self.charmap))

        #call the host objects print method
        ( lambda: print_cb( message,False ) )()
        message = ''
      if self.newline: break
      time.sleep(0.01)
    #store it on the command buffer for lookup using arrows
    self.command_buffer.append(self.line)
    return self.line

  def get_keystroke( self ,cursor ):
    ''' Check keyboard input and return regardless of key pressed'''
    
    def string_subtract( container, subtractor ):
      '''Remove one string fron another'''

      start_index = container.find(subtractor)
      end_index   = start_index + len(subtractor)
      output = container[0:start_index] + container[end_index:len(container)]
      return output

    def string_update( base, update ):
      '''Take two strings and overlay the second onto the first'''

      bl = len(base)
      ul = len(update)

      if bl >= ul:
        output = update + base[ul:]
      else:
        output = update
      return output

    self.line = ''
    line_buff = ''

    while(True):
      ll = len(self.line)
      #if ther is a character on keyboard buffer
      if msvcrt.kbhit():
        #Kill the thread on stop signal
        if self.stop: return

        key = msvcrt.getwche()

        #################
        # Detect arrows #
        #################
        if key == u'\xe0':
          arrow_code = ord( msvcrt.getwche() )
          sys.stdout.write("\x08 \x08")

          if not(self.command_buffer) and arrow_code in[72,80]:
            continue

          #if allow is up clear line,clear the char character printed by echo, and print last message in circ buffer.
          #and print last message in circ buffer.Then rotate the buffer
          if arrow_code == 72:
            self.clear_chars( len(self.line) )
            sys.stdout.write( "%s%s"%( cursor,self.command_buffer[-1]))
            self.line = self.command_buffer[-1]
            self.command_buffer.rotate()

          #if arrow down, do the same and rotate counter clock wise.
          elif arrow_code == 80:
            self.clear_chars( len(self.line) )
            sys.stdout.write( "%s%s"%( cursor,self.command_buffer[-1]))
            self.line = self.command_buffer[-1]
            self.command_buffer.reverse()

          #if back arrow is pressed move the cursor and get a backup of current line
          elif arrow_code == 75:

            if not self.line: continue
            #the first time its pressed it saves original word in buffer
            if not line_buff: line_buff = self.line

           #clear  last char
            sys.stdout.write("\x08 \x08")

            #clear it from stored line
            self.line = self.line[:-1]

          #if front arrow is pressed restore the next character in line
          elif arrow_code == 77:

            #get the invisible chars
            text = string_subtract(line_buff,self.line)

            #if there are chars to be printed push them to stdout
            if text:
              sys.stdout.write( text[0] )
              self.line += text[0]
          continue

        #################
        # Detect Tab  #
        #################
        elif key == u'\t':
          #if there are characters in buffer auto-complete
          if line_buff:
            sys.stdout.write( "\r"+ cursor +line_buff )
            self.line = line_buff
          #else remove the tab char
          else:
            sys.stdout.write( "\r"+ cursor + self.line )

        #################
        # Detect Enter  #
        #################
        elif key == '\r':
          self.newline = True
          #clear the line buffer
          line_buff = ''
          return

        #####################
        # Detect Backspace  #
        #####################
        elif key == '\x08':
          #do not delete boundary chars
          if ll == 0: 
            sys.stdout.write( cursor[-1] )
            continue
          msvcrt.putwch(u' ')
          msvcrt.putwch(key)
          self.line = self.line[:-1]
        else:
          ekey = key.encode(self.charmap, 'replace')
          self.line += ekey
          #if the user has characters buffered and modified one update it
          if line_buff: line_buff = string_update(line_buff,self.line)

        time.sleep(0.01)
