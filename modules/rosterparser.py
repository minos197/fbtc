#!/usr/bin/env python

import threading

class RosterParser(threading.Thread):
    """ Class that parses a SLEEKXXMP roster to a dictionary for faster parsing and data sorting """
    
    def __init__(self, roster, callback):
        self.strrstr = str(roster)
        #Generate the roster oject
        self.roster =  {'jid':{},'name':{},'id':{},'msgs':{},'alias':{},'invalias':{}}
        self.complete = False
        #connect the callback that will pass the completed roster to caller
        self.cb = callback
        super(RosterParser, self).__init__()

    def build(self):
      """ Parse the row xml and extract information """
      rst = self.strrstr.split("</item>")

      #Assign a 
      idf = 0
      for i in rst:
        #ingore data that dot not contain user id's
        if "jid" not in i:
          continue

        #extract the information
        i        = i.replace("<item ","")
        i        = i.split(" subscription=")[0]
        jid,name = i.split(" name=")
        jid      = jid.replace("jid=\"","")
        jid      = jid.replace('"',"")
        name     = name.replace('"',"")

        #store it in roster with redundancy to make searching easier
        self.roster['jid'][name]  = jid
        self.roster['name'][jid]  = name
        self.roster['id'][name]   = idf
        self.roster['msgs'][name] = []

        #increment the user id for next user
        idf += 1

      #Initialize empty fields of roster
      self.roster['online'] = {}
      self.roster['notify'] = []

      #signal the proccess as complete
      self.complete = True

      #call the assigned callback
      self.cb()
      return self

    def get(self):
      """ Retrieve the roster if parsing is complete """

      if self.complete: return self.roster
      else: return None
