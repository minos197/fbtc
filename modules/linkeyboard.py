#!/usr/bin/env python

"""keyboard.py: A keyboard handler for python."""

from __future__ import print_function

__author__  = "Minos Galanakis"
__license__ = "LGPL"
__version__ = "2.1"
__email__   = "minos197@gmail.com"

#import standard libs
import os
import sys
import time
import threading
import termios
import fcntl
from collections import deque as cbuffer

class KeyboardHandler( ):

  def __init__( self , cb ):
    self.line      = ''
    self.cb        = cb
    self.newline   = False
    self.stop      = False
    self.set_charmap()
    self.command_buffer = cbuffer( maxlen= 50 )
    self.set_terminal()
    #print ("Linyux DEBUG")

  def set_terminal( self ):
    '''Backup current file descriptor configuration and modify it for the program needs'''

    fd = sys.stdin.fileno()
    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)

    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

    self.fd = fd
    self.oldterm =oldterm
    self.oldflags = oldflags

  def getwche( self ):
    '''Get the Unicode char of input '''

    arrows = ['U','D','R','L',]
    while True:
      try: 
        c = sys.stdin.read(2)
        #if the input is not an arrow key
        if '\x1b' not in c:
          #Detect unicode chars
          if len(c) == 2:
            #echo it if its a printable character
            sys.stdout.write(c)
          elif ord(c) in range(32,127):
            sys.stdout.write(c)
          break
        #if arrow keys are pressed the input needs to be read once more
        else:
          c = sys.stdin.read(2)
          #print("Debug2",repr(c))
          c = "[%s]"% arrows[ord(c[0]) - 65]
          break
      except IOError: 
        pass

    return  c

  def set_charmap( self, map = 'iso-8859-7' ):
    '''Legacy support not used in Linux'''

    self.charmap = map

  def quit( self ):
    '''Clean up routines'''

    #signal the thread to stop
    self.stop = True

    #restore terminal flags
    print("Cleanup")
    termios.tcsetattr( self.fd, termios.TCSAFLUSH, self.oldterm )
    fcntl.fcntl( self.fd, fcntl.F_SETFL, self.oldflags )

  def clear_chars( self, number ):
    ''' Clear previously typed chars in terminal'''

    whitespace = " "* (number+1)
    sys.stdout.write( '\r'+ whitespace +'\r' )

  def premptive_input( self ,message ):
    ''' Asks for user input but allows main program to inject text in it'''

    #print the prompt without adding a newline
    sys.stdout.write( message )

    #start the keyboard query manager as a thread
    self.newline   = False
    thread = threading.Thread(target=self.get_keystroke,args = message)
    thread.start() 

    #loop until user presses enter
    while True:

      inc_msg = self.cb()
      if inc_msg:
        self.clear_chars( len(self.line) )
        sys.stdout.flush()
        try:
          sys.stdout.flush()
          sys.stdout.write( "%s\n%s %s"%(inc_msg,message,self.line) )
        except:
          print ("Error",repr(inc_msg),repr(message),repr(self.line))
          sys.stdout.flush()
      
      if self.newline: break
      time.sleep(0.1)
    #do not store empty lines (consequtive enter press)  
    if len(self.line):
      #store it on the command buffer for lookup using arrows
      self.command_buffer.append(self.line)
    sys.stdout.flush()
    sys.stdout.write("\n")
    return self.line

  def get_keystroke( self ,cursor ):
    ''' Check keyboard input and return regardless of key pressed'''

    def string_subtract( container, subtractor ):
      '''Remove one string fron another'''

      start_index = container.find(subtractor)
      end_index   = start_index + len(subtractor)
      output = container[0:start_index] + container[end_index:len(container)]
      return output

    def string_update( base, update ):
      '''Take two strings and overlay the second onto the first'''

      bl = len(base)
      ul = len(update)

      if bl >= ul:
        output = update + base[ul:]
      else:
        output = update
      return output

    self.line = ''
    line_buff = ''

    while(True):
      
      ll = len(self.line)
      #if ther is a character on keyboard buffer
      if True:
        #Kill the thread on stop signal
        if self.stop: break

        key = self.getwche()
        #print ('Debug',repr(key))
        #################
        # Detect arrows #
        #################
        if len(key) == 3 :
          arrow_code = key[1]
          #sys.stdout.write("\x08 \x08")

          if not(self.command_buffer) and arrow_code in['U','D']:
            continue

          #if allow is up clear line,clear the char character printed by echo, and print last message in circ buffer.
          #and print last message in circ buffer.Then rotate the buffer
          if arrow_code == 'U':
            self.clear_chars( len(self.line) )
            sys.stdout.write( "%s%s"%( cursor,self.command_buffer[-1]))
            self.line = self.command_buffer[-1]
            self.command_buffer.rotate()

          #if arrow down, do the same and rotate counter clock wise.
          elif arrow_code == 'D':
            self.clear_chars( len(self.line) )
            sys.stdout.write( "%s%s"%( cursor,self.command_buffer[-1]))
            self.line = self.command_buffer[-1]
            self.command_buffer.reverse()

          #if back arrow is pressed move the cursor and get a backup of current line
          elif arrow_code == 'L':

            if not self.line: continue
            #the first time its pressed it saves original word in buffer
            if not line_buff: line_buff = self.line

           #clear  last char
            sys.stdout.write("\x08 \x08")

            #clear it from stored line
            self.line = self.line[:-1]

          #if front arrow is pressed restore the next character in line
          elif arrow_code == 'R':

            #get the invisible chars
            text = string_subtract(line_buff,self.line)

            #if there are chars to be printed push them to stdout
            if text:
              sys.stdout.write( text[0] )
              self.line += text[0]
          continue

        #elif len(key) > 1:
        #  print ('Debug',repr(key))
        #  continue
          
        #################
        # Detect Tab  #
        #################
        elif key == '\t':
          #if there are characters in buffer auto-complete
          if line_buff:
            sys.stdout.write( "\r"+ cursor +line_buff )
            self.line = line_buff
          #else remove the tab char
          else:
            sys.stdout.write( "\r"+ cursor + self.line )

        #################
        # Detect Enter  #
        #################
        elif key == '\n':
          self.newline = True
          #clear the line buffer
          line_buff = ''
          return

        #####################
        # Detect Backspace  #
        #####################
        elif key == '\x7f':
          #do not delete boundary chars
          if ll == 0: 
            continue
          sys.stdout.write('\x08 \x08')
          self.line = self.line[:-1]
        else:
          #ekey = key.encode(self.charmap, 'replace')
          self.line += key
          #if the user has characters buffered and modified one update it
          if line_buff: line_buff = string_update(line_buff,self.line)

        time.sleep(0.01)
        
    print("Exiting Thread")
    self.quit()
    return
